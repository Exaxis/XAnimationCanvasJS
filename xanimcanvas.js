(function(root){
	// Request Animation Frame shim code taken from
	// https://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
	root.requestAnimFrame = (function(){
		return  root.requestAnimationFrame || 
			root.webkitRequestAnimationFrame || 
			root.mozRequestAnimationFrame || 
			root.oRequestAnimationFrame || 
			root.msRequestAnimationFrame || 
			function(callback, element){
				root.setTimeout(callback, 1000 / 60);
			};
	})();
	
	function XAnimCanvas(id, setup, update, draw, resize, mouseDown, mouseUp, mouseMove){
		// --- Public Members ---
		
		// ---- Private Members ---
		var _bgCanvas;    // Canvas used for background drawing
		var _midCanvas;   // Canvas positioned between foreground and background
		var _foreCanvas;  // Canvas used for foreground drawing
		
		var _width;       // Current width of canvases
		var _height;      // Current height of canvases
		
		var _active = true;   // Boolean to determine if drawing / updating 
		                      // should proceed
		
		// --- Main Method Callbacks ---
		var _setupHandler = setup != null ? setup : null;
		var _updateHandler = update != null ? update : null;
		var _drawHandler = draw != null ? draw : null;
		var _resizeHandler = resize != null ? resize : null;
		var _mouseDownHandler = mouseDown != null ? mouseDown : null;
		var _mouseUpHandler = mouseUp != null ? mouseUp : null;
		var _mouseMoveHandler = mouseMove != null ? mouseMove : null;
		
		// --- Public Methods ---
		// Event handlers registration
		this.onSetup = function(s){
			_setupHandler = s;
		}
		
		this.onUpdate = function(u){
			_updateHandler = u;
		}
		
		this.onDraw = function(d){
			_drawHandler = d;
		}
		
		this.onResize = function(r){
			_resizeHandler = r;
		}
		
		this.onMouseDown = function(d){
			_mouseDownHandler = d;
		}
		
		this.onMouseUp = function(u){
			_mouseUpHandler = u;
		}
		
		this.onMouseMove = function(m){
			_mouseMoveHandler = m;
		}
		
		// Starting and stopping animation
		this.start = function(){
			_active = true;
			_animate();
		}
		
		this.stop = function(){
			_active = false;
		}
		
		// --- Private Methods ---
		var _init = function(){
			
			_bgCanvas = _createCanvas(5);
			_midCanvas = _createCanvas(6);
			_foreCanvas = _createCanvas(7);
		
		    if(id != null){
				var parent = document.getElementById(id);
				parent.appendChild(_bgCanvas);
				parent.appendChild(_midCanvas);
				parent.appendChild(_foreCanvas);
				parent.style.position = "relative";	
			} else {
				document.body.appendChild(this.canvas);
		    }
			
			_bgCanvas.width = _bgCanvas.offsetWidth;
			_bgCanvas.height = _bgCanvas.offsetHeight;
			
			_midCanvas.width = _midCanvas.offsetWidth;
			_midCanvas.height = _midCanvas.offsetHeight;
			
			_foreCanvas.width = _foreCanvas.offsetWidth;
			_foreCanvas.height = _foreCanvas.offsetHeight;
			
			_width = _bgCanvas.offsetWidth;
			_height = _bgCanvas.offsetHeight;
			
			window.addEventListener("resize", _resize, false);
			
			_foreCanvas.addEventListener("mousedown", _mouseDown, false);
			_foreCanvas.addEventListener("mouseup", _mouseUp, false);
			_foreCanvas.addEventListener("mousemove", _mouseMove, false);
			
			if(_setupHandler != null){
				_setupHandler(_width, _height);
			}
			
			_animate();
		}
		
		// Performs one frame of animating, i.e., updating logic and drawing
		var _animate = function(){
			if(_active){
				_update();
				_draw();
				
				root.requestAnimFrame(_animate);
			}
		}
		
		var _update = function(){
			if(_updateHandler != null){
				_updateHandler();
			}
		}
		
		var _draw = function(){
			if(_drawHandler != null){
				_drawHandler(_width, _height,
							 _foreCanvas.getContext("2d"),
							 _midCanvas.getContext("2d"),
							 _bgCanvas.getContext("2d"));
			}
		}
		
		// - Event Methods -
		var _resize = function(){
			_bgCanvas.width = _bgCanvas.offsetWidth;
			_bgCanvas.height = _bgCanvas.offsetHeight;
			
			_midCanvas.width = _midCanvas.offsetWidth;
			_midCanvas.height = _midCanvas.offsetHeight;
			
			_foreCanvas.width = _foreCanvas.offsetWidth;
			_foreCanvas.height = _foreCanvas.offsetHeight;
			
			_width = _bgCanvas.offsetWidth;
			_height = _bgCanvas.offsetHeight;
			
			if(_resizeHandler != null){
				_resizeHandler();
			}
		}
		
		var _mouseDown = function(event){
			if(_mouseDownHandler != null){
				var mousePos = _getMouseScreenPosition(event);
				
				_mouseDownHandler(mousePos);
			}
		}
		
		var _mouseUp = function(event){
			if(_mouseUpHandler != null){
				var mousePos = _getMouseScreenPosition(event);
				
				_mouseUpHandler(mousePos);
			}
		}
		
		var _mouseMove = function(event){
			if(_mouseMoveHandler != null){
				var mousePos = _getMouseScreenPosition(event);
				
				_mouseMoveHandler(mousePos);
			}
		}
		
		// - Helper Methods -
		function _getMouseScreenPosition(event){
			var rect = _foreCanvas.getBoundingClientRect();
			var x = event.clientX - rect.left;
    		var y = event.clientY - rect.top;
			return new XVector.Vec3(x, y);
		}
		
		function _createCanvas(z){
			var newCanvas = document.createElement('canvas');
			newCanvas.style.width = "100%";
			newCanvas.style.height = "100%";
			newCanvas.style.position = "absolute";
			newCanvas.style.top = "0";
			newCanvas.style.left = "0";
			newCanvas.style.zIndex = z;
			
			return newCanvas;
		}
		
		root.onload = function(){
			_init();
		}
	}
	
	root.XAnimCanvas = XAnimCanvas;
	
})(this);